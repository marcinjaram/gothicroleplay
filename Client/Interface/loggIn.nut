
Interface.LoggIn <- {
    window = GUI.Window(anx(Resolution.x/2-250), any(Resolution.y/2 - 300), anx(500), any(460), "MENU_INGAME.TGA", null, false)

    "show" : function() {
        window.setVisible(true);
    }

    "hide" : function() {
        window.setVisible(false);
    }
};

Interface.LoggIn.logo <- GUI.Draw(anx(70), any(-40), CFG.Hostname, Interface.LoggIn.window)
Interface.LoggIn.usernameDraw <- GUI.Draw(anx(20), any(100), _L("Username"), Interface.LoggIn.window)
Interface.LoggIn.passwordDraw <- GUI.Draw(anx(20), any(200), _L("Password"), Interface.LoggIn.window)

Interface.LoggIn.username <- GUI.Input(anx(20), any(120), anx(460), any(50), "DLG_CONVERSATION.TGA", "FONT_OLD_10_WHITE_HI.TGA", Input.Text, Align.Left, "...", 6, Interface.LoggIn.window)
Interface.LoggIn.password <- GUI.Input(anx(20), any(220), anx(460), any(50), "DLG_CONVERSATION.TGA", "FONT_OLD_10_WHITE_HI.TGA", Input.Password, Align.Left, "...", 6, Interface.LoggIn.window)

Interface.LoggIn.exitButton <- GUI.Button(anx(20), any(300), anx(210), any(50), "DLG_CONVERSATION.TGA", _L("Exit"), Interface.LoggIn.window)
Interface.LoggIn.loggInButton <- GUI.Button(anx(270), any(300), anx(210), any(50), "DLG_CONVERSATION.TGA", _L("Logg In"), Interface.LoggIn.window)
Interface.LoggIn.registerButton <- GUI.Button(anx(20), any(370), anx(460), any(50), "DLG_CONVERSATION.TGA", _L("Register"), Interface.LoggIn.window)

Interface.LoggIn.logo.setFont("FONT_OLD_20_WHITE_HI.TGA");
Interface.LoggIn.logo.setColor(200, 0, 0);
Interface.LoggIn.window.setColor(255, 0, 0);

if(CFG.LanguageSwitcher)
{
	Interface.LoggIn.lang <- {};

	foreach(langKey, lang in CFG.Languages)
		Interface.LoggIn.lang[langKey] <- GUI.Draw(anx(10) - anx(Resolution.x/2-250), any(Resolution.y/2 + 200 + 20*lang.layout), lang.text, Interface.LoggIn.window)
}

addEventHandler("onInit", function(){
	Camera.setPosition(2244, 1442, 2421);
    Interface.LoggIn.show();
	clearMultiplayerMessages();
	setKeyLayout(CFG.Languages[CFG.DefaultLanguage].layout);
    Interface.baseInterface(true,PLAYER_GUI.LOGGIN);
})

addEventHandler("GUI.onClick", function(self)
{
    if(Player.gui != PLAYER_GUI.LOGGIN)
        return;

	switch (self)
	{
		case Interface.LoggIn.exitButton:
			exitGame();
		break;

		case Interface.LoggIn.registerButton:
			Interface.LoggIn.hide();
			Interface.Register.show();
			Player.gui = PLAYER_GUI.REGISTER;
		break;

		case Interface.LoggIn.loggInButton:
			Player.packetLoggIn(Interface.LoggIn.username.getText(), md5(Interface.LoggIn.password.getText()));
		break;
	}

	if(!CFG.LanguageSwitcher)
		return;

	foreach(keyLang, _button in Interface.LoggIn.lang)
	{
		if(_button == self)
			setPlayerLanguage(keyLang);
	}
})


addEventHandler("onChangeLanguage", function(lang) {
	Interface.LoggIn.usernameDraw.setText(_L("Username"))
	Interface.LoggIn.passwordDraw.setText(_L("Password"))

	Interface.LoggIn.exitButton.setText(_L("Exit"))
	Interface.LoggIn.loggInButton.setText(_L("Logg In"))
	Interface.LoggIn.registerButton.setText(_L("Register"))
});